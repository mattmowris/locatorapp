angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $http, AccountList) {
    $scope.accounts = null;

    AccountList.getAccounts().then(function(response) {
      $scope.accounts = response.data;
      $scope.processAccounts();
    });

    $scope.processAccounts = function() {
      if(!$scope.accounts) return false;
      
      // for(var i in $scope.accounts) {
      //   console.log($scope.accounts[i])
      // }
      var acc = $scope.accounts[0];
      acc["name"] = "Test Update";
      $scope.updateAccount(acc);

    }

    $scope.updateAccount = function(account_param) {
      var endPoint = "http://localhost:3000/accounts/" + account_param["id"];
      $http.post(endPoint, account_param)
     .success(function(data,status,headers,config) {
       console.log("SUCCESS", data);
     })
     .error(function(data,status,headers,config) {
        console.log("ERROR", data);
     });
    }
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
